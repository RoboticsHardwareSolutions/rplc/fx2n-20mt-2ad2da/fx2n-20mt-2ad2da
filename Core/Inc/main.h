/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define OUT_Y10_Pin GPIO_PIN_2
#define OUT_Y10_GPIO_Port GPIOE
#define OUT_Y11_Pin GPIO_PIN_3
#define OUT_Y11_GPIO_Port GPIOE
#define INPUT_X04_Pin GPIO_PIN_0
#define INPUT_X04_GPIO_Port GPIOA
#define INPUT_X03_Pin GPIO_PIN_1
#define INPUT_X03_GPIO_Port GPIOA
#define AO_DA2_Pin GPIO_PIN_4
#define AO_DA2_GPIO_Port GPIOA
#define AO_DA1_Pin GPIO_PIN_5
#define AO_DA1_GPIO_Port GPIOA
#define SWITCH_AI0_Pin GPIO_PIN_6
#define SWITCH_AI0_GPIO_Port GPIOA
#define SWITCH_AI1_Pin GPIO_PIN_7
#define SWITCH_AI1_GPIO_Port GPIOA
#define INPUT_X05_Pin GPIO_PIN_4
#define INPUT_X05_GPIO_Port GPIOC
#define AI_AD1_Pin GPIO_PIN_0
#define AI_AD1_GPIO_Port GPIOB
#define AI_AD2_Pin GPIO_PIN_1
#define AI_AD2_GPIO_Port GPIOB
#define INPUT_X06_Pin GPIO_PIN_8
#define INPUT_X06_GPIO_Port GPIOE
#define RUN_SWITCH_Pin GPIO_PIN_12
#define RUN_SWITCH_GPIO_Port GPIOE
#define RS485_TX_Pin GPIO_PIN_10
#define RS485_TX_GPIO_Port GPIOB
#define RS485_TXB11_Pin GPIO_PIN_11
#define RS485_TXB11_GPIO_Port GPIOB
#define LED_ERROR_Pin GPIO_PIN_13
#define LED_ERROR_GPIO_Port GPIOB
#define INPUT_X02_Pin GPIO_PIN_15
#define INPUT_X02_GPIO_Port GPIOB
#define INPUT_X01_Pin GPIO_PIN_6
#define INPUT_X01_GPIO_Port GPIOC
#define INPUT_X00_Pin GPIO_PIN_7
#define INPUT_X00_GPIO_Port GPIOC
#define OUT_Y00_Pin GPIO_PIN_9
#define OUT_Y00_GPIO_Port GPIOC
#define OUT_Y01_Pin GPIO_PIN_8
#define OUT_Y01_GPIO_Port GPIOA
#define RS232_TX_Pin GPIO_PIN_9
#define RS232_TX_GPIO_Port GPIOA
#define RS232_RX_Pin GPIO_PIN_10
#define RS232_RX_GPIO_Port GPIOA
#define INPUT_X12_Pin GPIO_PIN_11
#define INPUT_X12_GPIO_Port GPIOC
#define INPUT_X11_Pin GPIO_PIN_12
#define INPUT_X11_GPIO_Port GPIOC
#define INPUT_X10_Pin GPIO_PIN_0
#define INPUT_X10_GPIO_Port GPIOD
#define INPUT_X07_Pin GPIO_PIN_1
#define INPUT_X07_GPIO_Port GPIOD
#define LED_RUN_Pin GPIO_PIN_2
#define LED_RUN_GPIO_Port GPIOD
#define OUT_Y03_Pin GPIO_PIN_7
#define OUT_Y03_GPIO_Port GPIOD
#define OUT_Y02_Pin GPIO_PIN_3
#define OUT_Y02_GPIO_Port GPIOB
#define OUT_Y05_Pin GPIO_PIN_4
#define OUT_Y05_GPIO_Port GPIOB
#define OUT_Y04_Pin GPIO_PIN_5
#define OUT_Y04_GPIO_Port GPIOB
#define OUT_Y07_Pin GPIO_PIN_6
#define OUT_Y07_GPIO_Port GPIOB
#define OUT_Y06_Pin GPIO_PIN_7
#define OUT_Y06_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
